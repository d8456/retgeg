package test.java;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.base.DataGetter;

public class testGetter {
	@Test
	void shouldReturnUnchanged() {
		DataGetter dataGetter = new DataGetter();
		String testString = "As Soon As Possibl";
		assertTrue(dataGetter.getCompiled(testString).equals(testString));
	}
	
	@Test
	void shouldReturnShortenedASAP() {
		DataGetter dataGetter = new DataGetter();
		String testString = "As Soon As Possible";
		assertTrue(dataGetter.getCompiled(testString).equals("asap"));
	}
}
