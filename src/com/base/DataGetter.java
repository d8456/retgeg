package com.base;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class DataGetter {
	//nie wiem czemu nie dzial z Entry<String, String>, SimpleEntry implementuje ten interfejs
	private final List<SimpleEntry<String, String>> shortToLong = new ArrayList<SimpleEntry<String, String>>(); 

	{
		shortToLong.add(new SimpleEntry<String, String>("ASAP","As Soon As Possible"));
		shortToLong.add(new SimpleEntry<String, String>("DIY","Do It Yourself"));
		shortToLong.add(new SimpleEntry<String, String>("FAQ","Frequently Asked Questions"));
		shortToLong.add(new SimpleEntry<String, String>("HR","Human Resources"));
		shortToLong.add(new SimpleEntry<String, String>("OMW","On My Way"));
		shortToLong.add(new SimpleEntry<String, String>("POV","Point Of View"));
		shortToLong.add(new SimpleEntry<String, String>("TBA","To Be Announced"));
	}
	
	public String getCompiled(String string) {
		var originalString = string;
		string = string.trim().toLowerCase();
		for(Entry<String, String> shortLong : shortToLong) {
			string = string.replaceAll(shortLong.getValue().toLowerCase(), shortLong.getKey().toLowerCase());
		}
		return string.toLowerCase().equals(originalString.toLowerCase())? originalString : string;
	}
	
	public String getResolved(String string) {
		var originalString = string;
		string = string.trim().toLowerCase();
		for(Entry<String, String> shortLong : shortToLong) {
			string = string.replaceAll(shortLong.getKey().toLowerCase(), shortLong.getValue().toLowerCase());
		}
		return string.toLowerCase().equals(originalString.toLowerCase())? originalString : string;
	}
}
